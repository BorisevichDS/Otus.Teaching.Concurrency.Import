﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Threading.Tasks;

namespace WebApiConsole
{
    /// <summary>
    /// WebApi клиент.
    /// </summary>
    internal class Client
    {
        private readonly IConfiguration configuration;
        private readonly Gateway gateway;

        public Client()
        {
            this.configuration = new ConfigurationBuilder()
                                     .SetBasePath(Directory.GetCurrentDirectory())
                                     .AddJsonFile("AppSettings.json")
                                     .Build();

            this.gateway = new Gateway();
        }

        /// <summary>
        /// Найти контакта.
        /// </summary>
        /// <returns>Асинхронная операция.</returns>
        private async Task FindCustomerAsync()
        {
            try
            {
                while (true)
                {
                    Console.Write("Введите идентификатор контакта для поиска: ");
                    string input = Console.ReadLine();

                    if (int.TryParse(input, out int id))
                    {
                        string url = this.configuration.GetValue<string>("ApiUrl");
                        var result = await this.gateway.FindCustomerAsync(url, id);

                        if (result != null)
                        {
                            Console.WriteLine(result.ToString());
                        }
                        else
                        {
                            Console.WriteLine($"Контакт с идентификатором {id} не найден.");
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Введены некорректные данные. Идентификатор контакта для поиска должен быть целым числом.");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"В процессе выполнения программы произошла ошибка с описанием: {e.Message}");
            }
            Console.ReadKey();

        }

        /// <summary>
        /// Запустить клиент.
        /// </summary>
        /// <returns>Асинхронная операция.</returns>
        public async Task Run()
        {
            await FindCustomerAsync();
        }
    }
}
