﻿using System.Threading.Tasks;

namespace WebApiConsole
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Client client = new Client();
            await client.Run();
        }
    }
}
