﻿using Core.Entities;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace WebApiConsole
{
    /// <summary>
    /// Шлюз для работы с Api.
    /// </summary>
    internal class Gateway
    {
        private readonly HttpClient client;

        public Gateway()
        {
            this.client = new HttpClient();
        }

        /// <summary>
        /// Найти контакт.
        /// </summary>
        /// <param name="url">URL API.</param>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns></returns>
        public async Task<CustomerDto> FindCustomerAsync(string url, int id)
        {
            var response = await client.GetAsync($"{url}/customers/{id}");
            CustomerDto dto = null;

            if (response.IsSuccessStatusCode)
            {
                string result = await response.Content.ReadAsStringAsync();
                dto = JsonSerializer.Deserialize<CustomerDto>(result, new JsonSerializerOptions());
            }

            return dto;
        }

    }
}
