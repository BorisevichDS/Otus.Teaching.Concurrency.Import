﻿using System.Collections.Generic;
using Core.Entities;
using Core.Parsers;
using System.Xml.Serialization;
using System.IO;
using DataGenerator.Dto;

namespace DataAccess.Parsers
{
    /// <summary>
    /// Xml парсер.
    /// </summary>
    public class XmlParser : IDataParser<List<Customer>>
    {
        private readonly string dataFilePath;

        public XmlParser(string dataFilePath)
        {
            this.dataFilePath = dataFilePath;
        }

        /// <summary>
        /// Парсит файл с данными контактов.
        /// </summary>
        /// <returns>Список контактов.</returns>
        public List<Customer> Parse()
        {
            using StreamReader stream = new StreamReader(this.dataFilePath);
            XmlSerializer serializer = new XmlSerializer(typeof(CustomersList));
            CustomersList customersList = (CustomersList)serializer.Deserialize(stream);

            return customersList?.Customers;
        }
    }
}