﻿using Core.Entities;
using Core.Parsers;
using ServiceStack;
using ServiceStack.Text;
using System.Collections.Generic;
using System.IO;

namespace DataAccess.Parsers
{
    /// <summary>
    /// CSV парсер.
    /// </summary>
    public class CsvParser : IDataParser<List<Customer>>
    {
        private readonly string dataFilePath;
        private CsvSerializer csvSerializer;

        public CsvParser(string dataFilePath)
        {
            this.dataFilePath = dataFilePath;
            this.csvSerializer = new CsvSerializer();
        }

        /// <summary>
        /// Парсит файл с данными контактов.
        /// </summary>
        /// <returns>Список контактов.</returns>
        public List<Customer> Parse() => File.ReadAllText(this.dataFilePath).FromCsv<List<Customer>>();
    }
}
