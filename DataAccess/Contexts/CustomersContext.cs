﻿using Microsoft.EntityFrameworkCore;
using Core.Entities;

namespace DataAccess.Contexts
{
    /// <summary>
    /// Контекст для доступа к данным.
    /// </summary>
    public class CustomersContext : DbContext
    {
        public CustomersContext(DbContextOptions<CustomersContext> options) : base(options)
        {
        }

        /// <summary>
        /// Получает или задает данные контактов.
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var customerEntity = modelBuilder.Entity<Customer>();

            customerEntity.ToTable("customers");
            customerEntity.HasKey(k => k.Id).HasName("id");

            customerEntity.Property(p => p.Id).ValueGeneratedNever();
            customerEntity.Property(p => p.Email).HasColumnName("email").HasMaxLength(100);
            customerEntity.Property(p => p.Phone).HasColumnName("phone").HasMaxLength(50);
            customerEntity.Property(p => p.FullName).HasColumnName("fullname").HasMaxLength(200);
        }
    }
}