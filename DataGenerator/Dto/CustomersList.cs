﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Core.Entities;

namespace DataGenerator.Dto
{
    [XmlRoot("Customers")]
    public class CustomersList
    {
        public List<Customer> Customers { get; set; }
    }
}