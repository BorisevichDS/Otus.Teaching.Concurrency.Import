using Core.DataGenerators;
using DataGenerator.Generators;

namespace DataGenerator.Factories
{
    /// <summary>
    /// ������ ����������� ������.
    /// </summary>
    public static class DataGeneratorFactory
    {
        public static IDataGenerator GetXmlDataGenerator(string fileName, int dataCount)
        {
            return new XmlDataGenerator(fileName, dataCount);
        }

        public static IDataGenerator GetCsvDataGenerator(string fileName, int dataCount)
        {
            return new CsvDataGenerator(fileName, dataCount);
        }
    }
}