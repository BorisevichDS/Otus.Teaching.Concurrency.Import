using Core.DataGenerators;
using DataGenerator.Dto;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DataGenerator.Generators
{
    public class XmlDataGenerator : IDataGenerator
    {
        private readonly string fileName;
        private readonly int dataCount;

        public XmlDataGenerator(string fileName, int dataCount)
        {
            this.fileName = fileName;
            this.dataCount = dataCount;
        }

        /// <summary>
        /// ���������� ������.
        /// </summary>
        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(this.dataCount);
            using var stream = File.Create(this.fileName);
            new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
            {
                Customers = customers
            });
        }

        /// <summary>
        /// ���������� ���������� ������.
        /// </summary>
        /// <returns>����������� ��������.</returns>
        public async Task GenerateAsync()
        {
            await Task.Run(() =>
            {
                var customers = RandomCustomerGenerator.Generate(this.dataCount);
                using var stream = File.Create(this.fileName);
                new XmlSerializer(typeof(CustomersList)).Serialize(stream, new CustomersList()
                {
                    Customers = customers
                });
            });
        }
    }
}