﻿using Core.DataGenerators;
using Core.Entities;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DataGenerator.Generators
{
    /// <summary>
    /// Генератор данных в формате CSV.
    /// </summary>
    public class CsvDataGenerator : IDataGenerator
    {
        private readonly string fileName;
        private readonly int dataCount;

        public CsvDataGenerator(string fileName, int dataCount)
        {
            this.fileName = fileName;
            this.dataCount = dataCount;
        }

        /// <summary>
        /// Генерирует данные.
        /// </summary>
        public void Generate()
        {
            List<Customer> customers = RandomCustomerGenerator.Generate(this.dataCount);
            using StreamWriter writer = new StreamWriter(this.fileName);

            foreach (Customer c in customers)
            {
                writer.WriteLine(c.ToString());
            }
        }


        /// <summary>
        /// Асинхронно генерирует данные.
        /// </summary>
        /// <returns>Асинхронная операция.</returns>
        public async Task GenerateAsync()
        {
            List<Customer> customers = RandomCustomerGenerator.Generate(this.dataCount);
            using StreamWriter writer = new StreamWriter(this.fileName);

            foreach (Customer c in customers)
            {
                await writer.WriteLineAsync(c.ToString());
            }
        }


    }
}
