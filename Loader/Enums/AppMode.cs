﻿namespace Loader.Enums
{
    /// <summary>
    /// Режим работы приложения.
    /// </summary>
    internal enum AppMode
    {
        /// <summary>
        /// Режим генерации файла с данными.
        /// </summary>
        Generate,

        /// <summary>
        /// Режим загрузки файла с данными.
        /// </summary>
        Load
    }
}