using Core.Entities;
using Core.Loaders;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Loader.Loaders
{
    internal class ThreadPoolDataLoader : IDataLoader<List<Customer>>
    {
        private readonly int threadCount;
        private static readonly AutoResetEvent resetEvent = new AutoResetEvent(false);
        private readonly ICustomerRepository repository;
        private static object locker = new object();

        public ThreadPoolDataLoader(int threadCount, ICustomerRepository repository)
        {
            this.threadCount = threadCount;
            this.repository = repository;
        }

        /// <summary>
        /// Метод загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        public void LoadData(List<Customer> data)
        {
            int batchSize = (int)Math.Ceiling((double)data.Count / threadCount);
            
            for (int i = 0; i < threadCount; i++)
            {
                List<Customer> batch = data.Skip(i * batchSize).Take(batchSize).ToList();
                ThreadPool.QueueUserWorkItem(LoadBatch, batch);
                resetEvent.WaitOne();
            }
        }

        /// <summary>
        /// Метод загружает порцию данных контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        private void LoadBatch(object threadContext)
        {
            List<Customer> batch = threadContext as List<Customer>;
            lock (locker)
            {
                this.repository.AddRange(batch);
            }

            resetEvent.Set();
        }

        /// <summary>
        /// Метод асинхронно загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}