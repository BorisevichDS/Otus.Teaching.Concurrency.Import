using Core.Entities;
using Core.Loaders;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Loader.Loaders
{
    /// <summary>
    /// Многопоточный загрузчик данных.
    /// </summary>
    internal class ThreadDataLoader : IDataLoader<List<Customer>>
    {
        private readonly int threadCount;
        private readonly ICustomerRepository repository;
        static object locker = new object();

        public ThreadDataLoader(int threadCount, ICustomerRepository repository)
        {
            this.threadCount = threadCount;
            this.repository = repository;
        }

        /// <summary>
        /// Метод загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        public void LoadData(List<Customer> data)
        {
            int batchSize = (int)Math.Ceiling((double)data.Count / threadCount);

            for (int i = 0; i < threadCount; i++)
            {
                List<Customer> batch = data.Skip(i * batchSize).Take(batchSize).ToList();
                Thread t = new Thread(() => LoadBatch(batch));
                t.Name = i.ToString();
                t.Start();
                t.Join();
            }
        }

        /// <summary>
        /// Метод загружает порцию данных контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        private void LoadBatch(List<Customer> data)
        {
            lock (locker)
            {
                this.repository.AddRange(data);
            }
        }

        /// <summary>
        /// Метод асинхронно загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}
