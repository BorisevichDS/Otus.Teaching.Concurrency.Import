﻿using Core.Entities;
using Core.Loaders;
using Core.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Loader.Loaders
{
    internal class AsyncDataLoader : IDataLoader<List<Customer>>
    {
        private readonly ICustomerRepository repository;
        private static int numberOfAttemptions = 3;

        public AsyncDataLoader(ICustomerRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Метод асинхронно загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        public async Task LoadDataAsync(List<Customer> data)
        {
            try
            {
                await this.repository.AddRangeAsync(data);
            }
            catch (Exception) when (numberOfAttemptions > 0)
            {
                if (numberOfAttemptions == 0)
                {
                    return;
                }
                else
                {
                    numberOfAttemptions -= 1;
                    await LoadDataAsync(data);
                }
            }
        }

        /// <summary>
        /// Метод загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        public void LoadData(List<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}
