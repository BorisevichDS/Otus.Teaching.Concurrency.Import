using Core.Entities;
using Core.Loaders;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Loader.Loaders
{
    public class FakeDataLoader: IDataLoader<List<Customer>>
    {
        /// <summary>
        /// Метод загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        public void LoadData(List<Customer> data)
        {
            Console.WriteLine("Loading data...");
            Thread.Sleep(10000);
            Console.WriteLine("Loaded data...");
        }

        /// <summary>
        /// Метод асинхронно загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        public Task LoadDataAsync(List<Customer> data)
        {
            throw new NotImplementedException();
        }
    }
}