﻿using Loader.Enums;

namespace Loader
{
    /// <summary>
    /// Конфигурация приложения.
    /// </summary>
    internal class AppConfiguration
    {
        /// <summary>
        /// Режим работы приложения.
        /// </summary>
        public AppMode Mode { get; }

        /// <summary>
        /// Имя файла с данными.
        /// </summary>
        public string DataFileName { get; }

        /// <summary>
        /// Формат файла.
        /// </summary>
        public FileFormat Format { get; }

        public AppConfiguration(AppMode mode, string dataFileName, FileFormat format)
        {
            this.Mode = mode;
            this.DataFileName = dataFileName;
            this.Format = format;
        }
    }
}
