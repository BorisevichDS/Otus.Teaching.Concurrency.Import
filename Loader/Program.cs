﻿using Core.DataGenerators;
using Core.Entities;
using Core.Loaders;
using Core.Parsers;
using Core.Repositories;
using DataAccess.Contexts;
using DataAccess.Parsers;
using DataAccess.Repositories;
using DataGenerator.Factories;
using Loader.Enums;
using Loader.Loaders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Loader
{
    class Program
    {
        private static IConfiguration _configuration;

        static async Task Main(string[] args)
        {
            try
            {
                // Парсим аргументы
                AppConfiguration appConfiguration = ParseArguments(args);

                if (appConfiguration != null)
                {
                    // Зачитываем файл с настройками.
                    _configuration = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("AppSettings.json")
                            .Build();

                    if (appConfiguration.Mode == AppMode.Generate)
                    {
                        await GenerateCustomersDataFileAsync(appConfiguration.DataFileName, appConfiguration.Format);
                    }
                    else if (appConfiguration.Mode == AppMode.Load)
                    {
                        List<Customer> customers = ParseData(appConfiguration.DataFileName, appConfiguration.Format);

                        int threadCount = _configuration.GetSection("DataLoad").GetValue<int>("ThreadCount");

                        LoadDataByThread(threadCount, customers);
                        LoadDataByThreadPool(threadCount, customers);
                        await LoadDataByTaskAsync(customers);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Метод разибрает параметры переданные в приложение. 
        /// </summary>
        /// <param name="args">Массив входных параметров.</param>
        static AppConfiguration ParseArguments(string[] args)
        {
            StringBuilder message = new StringBuilder();
            AppConfiguration appConfiguration = null;

            if (args.Length == 0)
            {
                message.AppendLine("Некорректный набор параметров. Для получения справки по использованию приложения укажите ключ -help.");
            }
            else if (args[0] == "-help")
            {
                message.AppendLine($"Программа для генерации и загрузки данных из файла.");
                message.AppendLine();
                message.AppendLine("Использование: loader.exe [mode] [data-file-path] [format]");
                message.AppendLine();
                message.AppendLine("mode:");
                message.AppendLine("  Режим работы приложения:");
                message.AppendLine("    generate - режим генерации файла с данными.");
                message.AppendLine("    loaddata - режим загрузки данных из файла с данными.");
                message.AppendLine();
                message.AppendLine("data-file-path:");
                message.AppendLine("  Полный путь к файлу с тестовыми данными.");
                message.AppendLine();
                message.AppendLine("format:");
                message.AppendLine("  Формат файла:");
                message.AppendLine("    xml - формат файла в формате xml.");
                message.AppendLine("    csv - формат файла в формате csv.");
            }
            else
            {
                if (args.Length == 3)
                {
                    string mode = args[0];
                    AppMode appMode = mode switch
                    {
                        "generate" => AppMode.Generate,
                        "loaddata" => AppMode.Load,
                        _ => throw new NotSupportedException($"Режим работы '{mode}' не поддерживается."),
                    };

                    string fileName = args[1];
                    string fileFormat = args[2];

                    FileFormat format = fileFormat switch
                    {
                        "xml" => FileFormat.xml,
                        "csv" => FileFormat.csv,
                        _ => throw new NotSupportedException($"Формат файла '{fileFormat}' не поддерживается."),
                    };

                    appConfiguration = new AppConfiguration(appMode, fileName, format);
                }
                else
                {
                    message.AppendLine("Некорректный набор параметров. Для получения справки по использованию приложения укажите ключ -help.");
                }
            }

            Console.WriteLine(message);
            return appConfiguration;
        }

        /// <summary>
        /// Генерирует файл.
        /// </summary>
        /// <param name="fileName">Имя файла с данными.</param>
        /// <param name="format">Формат файла с данными.</param>
        /// <returns>Асинхронная операция.</returns>
        static async Task GenerateCustomersDataFileAsync(string fileName, FileFormat format)
        {
            var section = _configuration.GetSection("DataGeneration");
            string generationType = section.GetValue<string>("DataFileGenerationType");
            int count = section.GetValue<int>("DataCount");

            if (string.IsNullOrWhiteSpace(generationType))
            {
                throw new ArgumentException("Некорректный AppSettings.json. Значение параметра не указано.", "DataFileGenerationType");
            }

            switch (generationType)
            {
                case "internal":
                    await GenerateCustomersDataFileByMethodAsync(fileName, count, format);
                    break;
                case "external":
                    GenerateCustomersDataFileByExternalApplication(fileName, count, format);
                    break;
            }
        }

        /// <summary>
        /// Генерирурет файл с данными через вызов метода.
        /// </summary>
        /// <param name="dataFileName">Имя файла с данными.</param>
        /// <param name="dataCount">Количество генериуремых данных.</param>
        /// <param name="format">Формат файла с данными.</param>
        /// <returns>Асинхронная операция.</returns>
        static async Task GenerateCustomersDataFileByMethodAsync(string dataFileName, int dataCount, FileFormat format)
        {
            string fileName = $"{dataFileName}.{format}";

            IDataGenerator generator = format switch
            {
                FileFormat.xml => DataGeneratorFactory.GetXmlDataGenerator(fileName, dataCount),
                FileFormat.csv => DataGeneratorFactory.GetCsvDataGenerator(fileName, dataCount),
                _ => null
            };

            await generator.GenerateAsync();
            Console.WriteLine($"Generated file '{fileName}' by method.");
        }

        /// <summary>
        /// Генерирурет файл с данными через вызов стороннего приложения.
        /// </summary>
        /// <param name="dataFileName">Имя файла с данными.</param>
        /// <param name="dataCount">Количество генериуремых данных.</param>
        /// <param name="format">Формат файла с данными.</param>
        static void GenerateCustomersDataFileByExternalApplication(string dataFileName, int dataCount, FileFormat format)
        {
            var section = _configuration.GetSection("DataGeneration");

            string applicationPath = section.GetValue<string>("ExternalGeneratorPath");
            string applicationName = section.GetValue<string>("ExternalGeneratorApplicationName");

            if (string.IsNullOrWhiteSpace(applicationPath))
            {
                throw new ArgumentException("Некорректный AppSettings.json. Значение параметра не указано.", "ExternalGeneratorPath");
            }

            if (string.IsNullOrWhiteSpace(applicationName))
            {
                throw new ArgumentException("Некорректный AppSettings.json. Значение параметра не указано.", "ExternalGeneratorApplication");
            }

            ProcessStartInfo processStartInfo = new ProcessStartInfo()
            {
                FileName = applicationPath + applicationName,
                Arguments = $"{dataFileName} {format} {dataCount}",
                CreateNoWindow = true,
            };
            Process.Start(processStartInfo).WaitForExit();

            Console.WriteLine($"Generated file '{dataFileName}' by external application.");
        }

        /// <summary>
        /// Метод загружает данные с использованием класса Thread.
        /// </summary>
        /// <param name="threadCount">Количество потоков.</param>
        /// <param name="customers">Контакты для загрузки.</param>
        static void LoadDataByThread(int threadCount, List<Customer> customers)
        {
            Stopwatch stopwatch = new Stopwatch();
            string connectionString = _configuration.GetSection("DataLoad").GetConnectionString("ThreadDataLoader");

            var dbContextOptions = new DbContextOptionsBuilder<CustomersContext>()
                                        .UseSqlite(connectionString)
                                        .EnableSensitiveDataLogging()
                                        .Options;

            CustomersContext context = new CustomersContext(dbContextOptions);
            ICustomerRepository repository = new CustomersRepository(context);
            IDataLoader<List<Customer>> loader = new ThreadDataLoader(threadCount, repository);

            stopwatch.Start();
            loader.LoadData(customers);
            stopwatch.Stop();

            Console.WriteLine($"ThreadDataLoader execution time is {stopwatch.ElapsedMilliseconds} ms.");
        }

        /// <summary>
        /// Метод загружает данные с использованием класса ThreadPool.
        /// </summary>
        /// <param name="threadCount">Количество потоков.</param>
        /// <param name="customers">Контакты для загрузки.</param>
        static void LoadDataByThreadPool(int threadCount, List<Customer> customers)
        {
            Stopwatch stopwatch = new Stopwatch();
            string connectionString = _configuration.GetSection("DataLoad").GetConnectionString("ThreadPoolDataLoader");

            var dbContextOptions = new DbContextOptionsBuilder<CustomersContext>()
                            .UseSqlite(connectionString)
                            .EnableSensitiveDataLogging()
                            .Options;

            CustomersContext context = new CustomersContext(dbContextOptions);
            ICustomerRepository repository = new CustomersRepository(context);
            IDataLoader<List<Customer>> loader = new ThreadPoolDataLoader(threadCount, repository);

            stopwatch.Start();
            loader.LoadData(customers);
            stopwatch.Stop();

            Console.WriteLine($"ThreadPoolDataLoader execution time is {stopwatch.ElapsedMilliseconds} ms.");
        }

        /// <summary>
        /// Метод загружает данные с использованием Task.
        /// </summary>
        /// <param name="customers">Контакты для загрузки.</param>
        /// <param name="debug">Флаг отладки.</param>
        static async Task LoadDataByTaskAsync(List<Customer> customers)
        {
            Stopwatch stopwatch = new Stopwatch();
            string connectionString = _configuration.GetSection("DataLoad").GetConnectionString("AsyncDataLoader");

            var dbContextOptions = new DbContextOptionsBuilder<CustomersContext>()
                            .UseSqlite(connectionString)
                            .EnableSensitiveDataLogging()
                            .Options;

            CustomersContext context = new CustomersContext(dbContextOptions);
            ICustomerRepository repository = new CustomersRepository(context);
            IDataLoader<List<Customer>> loader = new AsyncDataLoader(repository);

            stopwatch.Start();
            await loader.LoadDataAsync(customers);
            stopwatch.Stop();

            Console.WriteLine($"AsyncDataLoader execution time is {stopwatch.ElapsedMilliseconds} ms.");
        }

        /// <summary>
        /// Парсит файл с данными.
        /// </summary>
        /// <param name="dataFileName">Имя файла с данными.</param>
        /// <param name="format">Формат файла с данными.</param>
        static List<Customer> ParseData(string dataFileName, FileFormat format)
        {
            IDataParser<List<Customer>> parser = format switch
            {
                FileFormat.xml => new XmlParser(dataFileName),
                FileFormat.csv => new CsvParser(dataFileName),
                _ => null
            };

            List<Customer> customers = parser.Parse();
            Console.WriteLine($"Считано из файла {customers.Count} контактов.");
            return customers;
        }
    }
}