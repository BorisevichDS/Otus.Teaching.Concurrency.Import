## Проект для демонстрации различных вариантов параллельной загрузки данных.

### Loader

Консольное приложение, которое должно сгенирировать файл с данными и запустить загрузку из него через реализацию `IDataLoader`.

### DataGenerator

Библиотека, в которой должна определена логика генерации файла с данными, в базовом варианте это XML.

### DataGenerator.Application

Консольное приложение, которое позволяет при запуске отдельно выполнить генерацию файла из `DataGenerator` библиотеки.

### DataAccess

Библиотека, в которой находится доступ к базе данных и файлу с данными.

### Core

Библиотека, в которой определены сущности БД и основные интерфейсы, которые реализуют другие компоненты.

### WebApi

Проект, реализующий Api для взаимодействия с базой данных.

### WebApiConsole 

Консольное приложение, которое позволяет отправлять запросы к API, реализованного в проекте `WebApi`. В рамках консольного приложения реализован только поиск контактов по ID.

#### Использование консольного приложения Loader.

Приложение Loader представляет собой консольное приложение. У приложения есть ряд параметров. Для того, чтобы получить информацию о параметрах приложения и какие значения оно может принимать необходимо передать приложению ключ -help.

``` cmd
loader.exe -help 
```

Так же приложение имеет конфигурационный файл `AppSettings.json` с настройками:
- `DataGeneration` - раздел, в котором содержатся настройки генератора данных.
    - `DataFileGenerationType` - определяет тип запуска генератора данных. Возможные значение: `internal` и `external`
    - `ExternalGeneratorPath` - полный путь к стороннему генератору данных (в конце пути должен быть указан /).
    - `ExternalGeneratorApplicationName` - имя стороннего генератора данных.
    - `DataCount` - количество строк для генерации.
- `DataLoad` - раздел, в котором содержатся настройки загрузчика данных.
    - `ThreadCount` - количество потоков загрузки данных.
    - `ConnectionStrings` - содержит строки подключения к базам данных. 

#### Использование консольного приложения WebApiConsole.
Приложение WebApiConsole представляет собой консольное приложение, которое позволяет отправлять запросы на API. Приложение имеет конфигурационный файл `AppSettings.json` с настройками:
- `ApiUrl` - URL для обращения к API.