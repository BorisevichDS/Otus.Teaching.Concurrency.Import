namespace Core.Entities
{
    public class Customer
    {
        public Customer()
        { 
        
        }

        public Customer(int id, string fullName, string email, string phone)
        {
            Id = id;
            FullName = fullName;
            Email = email;
            Phone = phone;
        }

        /// <summary>
        /// �������� ��� ������ ������������� ��������.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// �������� ��� ������ ��� ��������.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// �������� ��� ������ ����� ����������� ����� ��������.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// �������� ��� ������ ����� �������� ��������.
        /// </summary>
        public string Phone { get; set; }

        public override string ToString()
        {
            return $"{this.Id};{this.FullName};{this.Email};{this.Phone}";
        }
    }
}