﻿using Core.Entities;
using System.Threading.Tasks;

namespace Core.Services
{
    /// <summary>
    /// Сервис для работы с данными контактов.
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Получить данные контакта по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор контакта.</param>
        /// <returns>Контакт.</returns>
        Task<Customer> GetAsync(int id);

        /// <summary>
        /// Зарегистрировать контакт.
        /// </summary>
        /// <param name="customer">Контакт.</param>
        /// <returns>Асинхронная операция.</returns>
        Task RegisterAsync(Customer customer);
    }
}
