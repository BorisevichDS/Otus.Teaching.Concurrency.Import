using System.Threading.Tasks;

namespace Core.DataGenerators
{
    public interface IDataGenerator
    {
        /// <summary>
        /// ���������� ������.
        /// </summary>
        void Generate();

        /// <summary>
        /// ���������� ���������� ������.
        /// </summary>
        /// <returns>����������� ��������.</returns>
        Task GenerateAsync();
    }
}