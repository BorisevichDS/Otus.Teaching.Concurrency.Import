﻿namespace Core.Parsers
{
    public interface IDataParser<T>
    {
        T Parse();
    }
}