using Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Core.Repositories
{
    /// <summary>
    /// ����������� ��� �������������� � ������� ���������.
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// �������� ������ �������� � ���� ������.
        /// </summary>
        /// <param name="customer">�������.</param>
        void Add(Customer customer);

        /// <summary>
        /// �������� ������ �������� � ���� ������.
        /// </summary>
        /// <param name="customer">�������.</param>
        /// <returns>����������� ��������.</returns>
        Task AddAsync(Customer customer);

        /// <summary>
        /// �������� ������ ��������� � ���� ������.
        /// </summary>
        /// <param name="customers">������ ���������.</param>
        void AddRange(List<Customer> customers);

        /// <summary>
        /// �������� ������ ��������� � ���� ������.
        /// </summary>
        /// <param name="customers">������ ���������.</param>
        /// <returns>����������� ��������.</returns>
        Task AddRangeAsync(List<Customer> customers);

        /// <summary>
        /// ����� ������� �� ��� ��������������.
        /// </summary>
        /// <param name="id">������������� ��������.</param>
        /// <returns>�������.</returns>
        Task<Customer> FindAsync(int id);
    }
}