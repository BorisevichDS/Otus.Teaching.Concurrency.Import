﻿using System.Threading.Tasks;

namespace Core.Loaders
{
    /// <summary>
    /// Загрузчик данных.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IDataLoader<T>
    {
        /// <summary>
        /// Метод загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные для загрузки.</param>
        void LoadData(T data);

        /// <summary>
        /// Метод асинхронно загружает данные контактов.
        /// </summary>
        /// <param name="data">Данные контактов.</param>
        /// <returns>Асинхронная операция.</returns>
        Task LoadDataAsync(T data);
    }
}