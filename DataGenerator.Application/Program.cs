﻿using Core.DataGenerators;
using DataGenerator.Application.Enums;
using DataGenerator.Factories;
using System;
using System.Text;
using System.Threading.Tasks;

namespace DataGenerator.Application
{
    class Program
    {
        static async Task Main(string[] args)
        {
            AppConfiguration appConfiguration = ParseArguments(args);

            if (appConfiguration != null)
            {
                string fileName = $"{appConfiguration.DataFileName}.{appConfiguration.Format}";

                IDataGenerator generator = appConfiguration.Format switch
                {
                    FileFormat.xml => DataGeneratorFactory.GetXmlDataGenerator(fileName, appConfiguration.DataCount),
                    FileFormat.csv => DataGeneratorFactory.GetCsvDataGenerator(fileName, appConfiguration.DataCount),
                    _ => null
                };

                await generator.GenerateAsync();
                Console.WriteLine($"Generated file '{fileName}'.");
            }
        }

        /// <summary>
        /// Метод разибрает параметры переданные в приложение. 
        /// </summary>
        /// <param name="args">Массив входных параметров.</param>
        static AppConfiguration ParseArguments(string[] args)
        {
            StringBuilder message = new StringBuilder();
            AppConfiguration appConfiguration = null;

            if (args.Length == 0)
            {
                message.AppendLine("Некорректный набор параметров. Для получения справки по использованию приложения укажите ключ -help.");
            }
            else if (args[0] == "-help")
            {
                message.AppendLine($"Программа для генерации данных.");
                message.AppendLine();
                message.AppendLine("Использование: DataGenerator.Application.exe [data-file-path] [format] [count]");
                message.AppendLine();
                message.AppendLine("data-file-path:");
                message.AppendLine("  Полный путь к файлу с тестовыми данными.");
                message.AppendLine();
                message.AppendLine("format:");
                message.AppendLine("  Формат файла:");
                message.AppendLine("    xml - формат файла в формате xml.");
                message.AppendLine("    csv - формат файла в формате csv.");
                message.AppendLine();
                message.AppendLine("count:");
                message.AppendLine("  Количество строк, которое необходимо сгенерировать.");
            }
            else
            {
                if (args.Length == 3)
                {
                    string fileName = args[0];
                    string fileFormat = args[1];
                    int count = int.Parse(args[2]);

                    FileFormat format = fileFormat switch
                    {
                        "xml" => FileFormat.xml,
                        "csv" => FileFormat.csv,
                        _ => throw new NotSupportedException($"Формат файла '{fileFormat}' не поддерживается."),
                    };

                    appConfiguration = new AppConfiguration(fileName, format, count);
                }
                else
                {
                    message.AppendLine("Некорректный набор параметров. Для получения справки по использованию приложения укажите ключ -help.");
                }
            }

            Console.WriteLine(message);
            return appConfiguration;
        }
    }
}