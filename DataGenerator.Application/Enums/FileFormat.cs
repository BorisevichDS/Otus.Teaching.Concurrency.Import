﻿namespace DataGenerator.Application.Enums
{
    /// <summary>
    /// Формат файла с данными.
    /// </summary>
    enum FileFormat
    {
        xml,
        csv
    }
}
