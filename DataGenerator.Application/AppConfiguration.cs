﻿using DataGenerator.Application.Enums;

namespace DataGenerator.Application
{
    /// <summary>
    /// Конфигурация приложения.
    /// </summary>
    internal class AppConfiguration
    {
        /// <summary>
        /// Имя файла с данными.
        /// </summary>
        public string DataFileName { get; }

        /// <summary>
        /// Формат файла.
        /// </summary>
        public FileFormat Format { get; }

        /// <summary>
        /// Количество строк, которое необходимо сгенерировать.
        /// </summary>
        public int DataCount { get; }

        public AppConfiguration(string dataFileName, FileFormat format, int count)
        {
            this.DataFileName = dataFileName;
            this.Format = format;
            this.DataCount = count;
        }
    }
}
